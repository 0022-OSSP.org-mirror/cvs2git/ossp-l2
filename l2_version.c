/*
**  l2_version.c -- Version Information for OSSP l2 (syntax: C/C++)
**  [automatically generated and maintained by GNU shtool]
*/

#ifdef _L2_VERSION_C_AS_HEADER_

#ifndef _L2_VERSION_C_
#define _L2_VERSION_C_

#define L2_VERSION 0x00920D

typedef struct {
    const int   v_hex;
    const char *v_short;
    const char *v_long;
    const char *v_tex;
    const char *v_gnu;
    const char *v_web;
    const char *v_sccs;
    const char *v_rcs;
} l2_version_t;

extern l2_version_t l2_version;

#endif /* _L2_VERSION_C_ */

#else /* _L2_VERSION_C_AS_HEADER_ */

#define _L2_VERSION_C_AS_HEADER_
#include "l2_version.c"
#undef  _L2_VERSION_C_AS_HEADER_

l2_version_t l2_version = {
    0x00920D,
    "0.9.13",
    "0.9.13 (08-Jun-2007)",
    "This is OSSP l2, Version 0.9.13 (08-Jun-2007)",
    "OSSP l2 0.9.13 (08-Jun-2007)",
    "OSSP l2/0.9.13",
    "@(#)OSSP l2 0.9.13 (08-Jun-2007)",
    "$Id: OSSP l2 0.9.13 (08-Jun-2007) $"
};

#endif /* _L2_VERSION_C_AS_HEADER_ */

