/*
**  OSSP l2 - Flexible Logging
**  Copyright (c) 2001-2005 Cable & Wireless <http://www.cw.com/>
**  Copyright (c) 2001-2005 The OSSP Project <http://www.ossp.org/>
**  Copyright (c) 2001-2005 Ralf S. Engelschall <rse@engelschall.com>
**
**  This file is part of OSSP l2, a flexible logging library which
**  can be found at http://www.ossp.org/pkg/lib/l2/.
**
**  Permission to use, copy, modify, and distribute this software for
**  any purpose with or without fee is hereby granted, provided that
**  the above copyright notice and this permission notice appear in all
**  copies.
**
**  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
**  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
**  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
**  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
**  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
**  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
**  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
**  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
**  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
**  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
**  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
**  SUCH DAMAGE.
**
**  l2_test.c: C API test suite
*/

#include <stdio.h>

#include "l2.h"

static void die(l2_env_t *env, l2_result_t rv, char *fmt, ...)
{
    va_list ap;
    char *error;

    va_start(ap, fmt);
    fprintf(stderr, "l2_test:ERROR: ");
    vfprintf(stderr, fmt, ap);
    error = l2_env_strerror(env, rv);
    fprintf(stderr, " (%s)\n", error);
    va_end(ap);
    exit(1);
}

static l2_result_t
formatter(l2_context_t *ctx, const char id, const char *param,
          char *bufptr, size_t bufsize, size_t *buflen, va_list *ap)
{
    int i;

    i = va_arg(*ap, int);
    sprintf(bufptr, "[%d/%s]", i, param);
    *buflen = strlen(bufptr);
    return L2_OK;
}

int main(int argc, char *argv[])
{
    l2_channel_t *ch;
#if 1
    char *spec;
#else
    l2_channel_t *chFilter;
    l2_channel_t *chPrefix;
    l2_channel_t *chBuffer;
    l2_channel_t *chFile;
    l2_channel_t *chSyslog;
    l2_channel_t *chSmtp;
#endif
    l2_result_t rv;
    l2_env_t *env;

    /* create environment */
    if ((rv = l2_env_create(&env)) != L2_OK)
        die(env, rv, "failed to create environment");
    if ((rv = l2_env_formatter(env, 'k', formatter, NULL)) != L2_OK)
        die(env, rv, "failed to configure formatter for %%x");
    if ((rv = l2_env_formatter(env, 'S', l2_util_fmt_dump, NULL)) != L2_OK)
        die(env, rv, "failed to configure formatter for %%S");
    if ((rv = l2_env_settimer(env, 20)) != L2_OK)
        die(env, rv, "failed to configure timer");

#if 1
    spec = "noop -> {"
           "  filter(regex=hecking, negate=0)"
           "  -> prefix(prefix=\"[%d-%m-%Y/%H:%M:%S] %L test[%P]: \", timezone=local)"
           "     -> buffer(size=800)"
           "        -> file(path=l2_test.log, trunc=0, perm=0644) ;"
/*
           "  syslog(ident=L2-Test, facility=user, "
           "         remotehost=localhost, logpid=1, target=remote)"
*/
           "}";
    fprintf(stderr, "configuring: %s\n", spec);
    if ((rv = l2_spec(&ch, env, "%s", spec)) != L2_OK)
        die(env, rv, "failed to parse specification");
#else
    /* create noop channel */
    if ((rv = l2_channel_create(&ch, env, "noop")) != L2_OK)
        die(env, rv, "failed to create noop channel");

    /* create prefix channel */
    if ((rv = l2_channel_create(&chPrefix, env, "prefix")) != L2_OK)
        die(env, rv, "failed to create prefix channel");
    if ((rv = l2_channel_configure(chPrefix, "prefix=\"%s\", timezone=local", "[%d-%m-%Y/%H:%M:%S] %L test[%P]: ")) != L2_OK)
        die(env, rv, "failed to configure prefix channel");

    /* create prefix channel */
    if ((rv = l2_channel_create(&chFilter, env, "filter")) != L2_OK)
        die(env, rv, "failed to create filter channel");
    if ((rv = l2_channel_configure(chFilter, "regex=hecking, negate=0")) != L2_OK)
        die(env, rv, "failed to configure filter channel");

    /* create buffer channel */
    if ((rv = l2_channel_create(&chBuffer, env, "buffer")) != L2_OK)
        die(env, rv, "failed to create buffer channel");
    if ((rv = l2_channel_configure(chBuffer, "size=800")) != L2_OK)
        die(env, rv, "failed to configure buffer channel");

    /* create file channel */
    if ((rv = l2_channel_create(&chFile, env, "file")) != L2_OK)
        die(env, rv, "failed to create file channel");
    if ((rv = l2_channel_configure(chFile, "path=l2_test.log, trunc=%d,perm=%d", 0, 0644)) != L2_OK)
        die(env, rv, "failed to configure file channel");
    if ((rv = l2_channel_levels(chFile, L2_LEVEL_UPTO(L2_LEVEL_INFO), L2_LEVEL_NONE)) != L2_OK)
        die(env, rv, "failed to level of smtp channel");

    /* create syslog channel */
    if ((rv = l2_channel_create(&chSyslog, env, "syslog")) != L2_OK)
        die(env, rv, "failed to create syslog channel");
    if ((rv = l2_channel_configure(chSyslog, "ident=L2-Test, facility=user, target=remote, remotehost=localhost, logpid=1")) != L2_OK)
        die(env, rv, "failed to configure syslog channel");
    if ((rv = l2_channel_levels(chSyslog, L2_LEVEL_UPTO(L2_LEVEL_ERROR), L2_LEVEL_ALL)) != L2_OK)
        die(env, rv, "failed to level of syslog channel");

    /* create smtp channel */
    if ((rv = l2_channel_create(&chSmtp, env, "smtp")) != L2_OK)
        die(env, rv, "failed to create smtp channel");
    if ((rv = l2_channel_configure(chSmtp, "rcpt=l2@localhost, host=localhost, port=25")) != L2_OK)
        die(env, rv, "failed to configure smtp channel");
    if ((rv = l2_channel_levels(chSmtp, L2_LEVEL_UPTO(L2_LEVEL_PANIC), L2_LEVEL_ALL)) != L2_OK)
        die(env, rv, "failed to level of smtp channel");

    /* build channel tree */
    if ((rv = l2_channel_link(ch, L2_LINK_CHILD, chFilter, chPrefix, chBuffer, chFile, NULL)) != L2_OK)
        die(env, rv, "failed to link file-related channels together as a child sequence");
    if ((rv = l2_channel_link(ch, L2_LINK_CHILD, chSyslog, NULL)) != L2_OK)
        die(env, rv, "failed to link syslog-related channels together as a child sequence");
#endif

    /* open channel tree */
    if ((rv = l2_channel_open(ch)) != L2_OK)
        die(env, rv, "failed to open channel tree");

    /* perform a few log operations */
    if ((rv = l2_channel_log(ch, L2_LEVEL_PANIC, "1: Checking localhost %s %{myparm}k <%{text}S><%{hex}S><%{base64}S>\n", "foo", 12345, "foo\1bar", 7, "foo\1bar", 7, "foo\1bar", 7)) != L2_OK)
        die(env, rv, "failed to log message #1 to channel");
    if ((rv = l2_channel_log(ch, L2_LEVEL_PANIC, "2: Checking\n")) != L2_OK)
        die(env, rv, "failed to log message #2 to channel");
    if ((rv = l2_channel_log(ch, L2_LEVEL_PANIC, "3: Checking localhost %s %{myparm}k <%{text}S><%{hex}S><%{base64}S>\n", "foo", 12345, "foo\1bar", 7, "foo\1bar", 7, "foo\1bar", 7)) != L2_OK)
        die(env, rv, "failed to log message #3 to channel");
    if ((rv = l2_channel_log(ch, L2_LEVEL_PANIC, "4: Checking\n")) != L2_OK)
        die(env, rv, "failed to log message #4 to channel");

    /* destroy channel tree */
    if ((rv = l2_channel_destroy(ch)) != L2_OK)
        die(env, rv, "failed to destroy channel tree");

    /* destroy environment */
    if ((rv = l2_env_destroy(env)) != L2_OK)
        die(env, rv, "failed to destroy environment");

    return 0;
}

