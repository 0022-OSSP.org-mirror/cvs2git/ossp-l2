dnl ##
dnl ##  OSSP l2 - Flexible Logging
dnl ##  Copyright (c) 2001-2005 Cable & Wireless <http://www.cw.com/>
dnl ##  Copyright (c) 2001-2005 The OSSP Project <http://www.ossp.org/>
dnl ##  Copyright (c) 2001-2005 Ralf S. Engelschall <rse@engelschall.com>
dnl ##
dnl ##  This file is part of OSSP l2, a flexible logging library which
dnl ##  can be found at http://www.ossp.org/pkg/lib/l2/.
dnl ##
dnl ##  Permission to use, copy, modify, and distribute this software for
dnl ##  any purpose with or without fee is hereby granted, provided that
dnl ##  the above copyright notice and this permission notice appear in all
dnl ##  copies.
dnl ##
dnl ##  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
dnl ##  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
dnl ##  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
dnl ##  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
dnl ##  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
dnl ##  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
dnl ##  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
dnl ##  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
dnl ##  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
dnl ##  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
dnl ##  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
dnl ##  SUCH DAMAGE.
dnl ##
dnl ##  l2.m4: Autoconf macro for locating OSSP l2 from within
dnl ##         configure.in of third-party software packages
dnl ##

dnl ##
dnl ##  Synopsis:
dnl ##  AC_CHECK_L2([MIN-VERSION [,          # minimum OSSP l2 version, e.g. 1.2.0
dnl ##               DEFAULT-WITH-L2 [,      # default value for --with-l2 option
dnl ##               DEFAULT-WITH-L2-TEST [, # default value for --with-l2-test option
dnl ##               EXTEND-VARS [,          # whether CFLAGS/LDFLAGS/etc are extended
dnl ##               ACTION-IF-FOUND [,      # action to perform if OSSP l2 was found
dnl ##               ACTION-IF-NOT-FOUND     # action to perform if OSSP l2 was not found
dnl ##               ]]]]]])
dnl ##  Examples:
dnl ##  AC_CHECK_L2(1.2.0)
dnl ##  AC_CHECK_L2(1.2.0,,,no,CFLAGS="$CFLAGS -DHAVE_L2 $L2_CFLAGS")
dnl ##  AC_CHECK_L2(1.2.0,yes,yes,yes,CFLAGS="$CFLAGS -DHAVE_L2")
dnl ##
dnl
dnl #   auxilliary macros
AC_DEFUN(_AC_L2_ERROR, [dnl
AC_MSG_RESULT([*FAILED*])
define(_ac_l2_line,dnl
"+------------------------------------------------------------------------+")
echo " _ac_l2_line" 1>&2
cat <<EOT | sed -e 's/^[[ 	]]*/ | /' -e 's/>>/  /' 1>&2
$1
EOT
echo " _ac_l2_line" 1>&2
undefine(_ac_l2_line)
exit 1
])
AC_DEFUN(_AC_L2_VERBOSE, [dnl
if test ".$verbose" = .yes; then
    AC_MSG_RESULT([  $1])
fi
])
dnl #   the user macro
AC_DEFUN(AC_CHECK_L2, [dnl
dnl
dnl #   prerequisites
AC_REQUIRE([AC_PROG_CC])dnl
AC_REQUIRE([AC_PROG_CPP])dnl
dnl
L2_CPPFLAGS=''
L2_CFLAGS=''
L2_LDFLAGS=''
L2_LIBS=''
AC_SUBST(L2_CPPFLAGS)
AC_SUBST(L2_CFLAGS)
AC_SUBST(L2_LDFLAGS)
AC_SUBST(L2_LIBS)
dnl #   command line options
AC_MSG_CHECKING(for OSSP l2)
_AC_L2_VERBOSE([])
AC_ARG_WITH(l2,dnl
[  --with-l2[=ARG]        Build with OSSP l2 Library  (default=]ifelse([$2],,yes,$2)[)],dnl
,dnl
with_l2="ifelse([$2],,yes,$2)"
)dnl
AC_ARG_WITH(l2-test,dnl
[  --with-l2-test         Perform OSSP l2 Sanity Test (default=]ifelse([$3],,yes,$3)[)],dnl
,dnl
with_l2_test="ifelse([$3],,yes,$3)"
)dnl
_AC_L2_VERBOSE([+ Command Line Options:])
_AC_L2_VERBOSE([    o --with-l2=$with_l2])
_AC_L2_VERBOSE([    o --with-l2-test=$with_l2_test])
dnl
dnl #   configuration
if test ".$with_l2" != .no; then
    _l2_subdir=no
    _l2_subdir_opts=''
    case "$with_l2" in
        subdir:* )
            _l2_subdir=yes
            changequote(, )dnl
            _l2_subdir_opts=`echo $with_l2 | sed -e 's/^subdir:[^ 	]*[ 	]*//'`
            with_l2=`echo $with_l2 | sed -e 's/^subdir:\([^ 	]*\).*$/\1/'`
            changequote([, ])dnl
            ;;
    esac
    _l2_version=""
    _l2_location=""
    _l2_type=""
    _l2_cppflags=""
    _l2_cflags=""
    _l2_ldflags=""
    _l2_libs=""
    if test ".$with_l2" = .yes; then
        #   via config script in $PATH
        changequote(, )dnl
        _l2_version=`(l2-config --version) 2>/dev/null |\
                      sed -e 's/^.*\([0-9]\.[0-9]*[ab.][0-9]*\).*$/\1/'`
        changequote([, ])dnl
        if test ".$_l2_version" != .; then
            _l2_location=`l2-config --prefix`
            _l2_type='installed'
            _l2_cppflags=`l2-config --cflags`
            _l2_cflags=`l2-config --cflags`
            _l2_ldflags=`l2-config --ldflags`
            _l2_libs=`l2-config --libs`
        fi
    elif test -d "$with_l2"; then
        with_l2=`echo $with_l2 | sed -e 's;/*$;;'`
        _l2_found=no
        #   via locally included source tree
        if test ".$_l2_subdir" = .yes; then
            _l2_location="$with_l2"
            _l2_type='local'
            _l2_cppflags="-I$with_l2"
            _l2_cflags="-I$with_l2"
            if test -f "$with_l2/ltconfig"; then
                _l2_ldflags="-L$with_l2/.libs"
            else
                _l2_ldflags="-L$with_l2"
            fi
            _l2_libs="-ll2"
            changequote(, )dnl
            _l2_version=`grep '^const char L2_Hello' $with_l2/l2_vers.c |\
                          sed -e 's;^.*Version[ 	]*\([0-9]*\.[0-9]*[.ab][0-9]*\)[ 	].*$;\1;'`
            changequote([, ])dnl
            _l2_found=yes
            ac_configure_args="$ac_configure_args --enable-subdir $_l2_subdir_opts"
            with_l2_test=no
        fi
        #   via config script under a specified directory
        #   (a standard installation, but not a source tree)
        if test ".$_l2_found" = .no; then
            for _dir in $with_l2/bin $with_l2; do
                if test -f "$_dir/l2-config"; then
                    test -f "$_dir/l2-config.in" && continue # l2-config in source tree!
                    changequote(, )dnl
                    _l2_version=`($_dir/l2-config --version) 2>/dev/null |\
                                  sed -e 's/^.*\([0-9]\.[0-9]*[ab.][0-9]*\).*$/\1/'`
                    changequote([, ])dnl
                    if test ".$_l2_version" != .; then
                        _l2_location=`$_dir/l2-config --prefix`
                        _l2_type="installed"
                        _l2_cppflags=`$_dir/l2-config --cflags`
                        _l2_cflags=`$_dir/l2-config --cflags`
                        _l2_ldflags=`$_dir/l2-config --ldflags`
                        _l2_libs=`$_dir/l2-config --libs`
                        _l2_found=yes
                        break
                    fi
                fi
            done
        fi
        #   in any subarea under a specified directory
        #   (either a special installation or a L2 source tree)
        if test ".$_l2_found" = .no; then
            changequote(, )dnl
            _l2_found=0
            for _file in x `find $with_l2 -name "l2.h" -type f -print`; do
                test .$_file = .x && continue
                _dir=`echo $_file | sed -e 's;[^/]*$;;' -e 's;\(.\)/$;\1;'`
                _l2_version=`($_dir/l2-config --version) 2>/dev/null |\
                              sed -e 's/^.*\([0-9]\.[0-9]*[ab.][0-9]*\).*$/\1/'`
                if test ".$_l2_version" = .; then
                    _l2_version=`grep '^#define L2_VERSION_STR' $_file |\
                                  sed -e 's;^#define[ 	]*L2_VERSION_STR[ 	]*"\([0-9]*\.[0-9]*[.ab][0-9]*\)[ 	].*$;\1;'`
                fi
                _l2_cppflags="-I$_dir"
                _l2_cflags="-I$_dir"
                _l2_found=`expr $_l2_found + 1`
            done
            for _file in x `find $with_l2 -name "libl2.[aso]" -type f -print`; do
                test .$_file = .x && continue
                _dir=`echo $_file | sed -e 's;[^/]*$;;' -e 's;\(.\)/$;\1;'`
                _l2_ldflags="-L$_dir"
                _l2_libs="-ll2"
                _l2_found=`expr $_l2_found + 1`
            done
            changequote([, ])dnl
            if test ".$_l2_found" = .2; then
                _l2_location="$with_l2"
                _l2_type="uninstalled"
            else
                _l2_version=''
            fi
        fi
    fi
    _AC_L2_VERBOSE([+ Determined Location:])
    _AC_L2_VERBOSE([    o path: $_l2_location])
    _AC_L2_VERBOSE([    o type: $_l2_type])
    if test ".$_l2_version" = .; then
        if test ".$with_l2" != .yes; then
             _AC_L2_ERROR([dnl
             Unable to locate OSSP l2 under $with_l2.
             Please specify the correct path to either a OSSP l2 installation tree
             (use --with-l2=DIR if you used --prefix=DIR for installing OSSP l2 in
             the past) or to a OSSP l2 source tree (use --with-l2=DIR if DIR is a
             path to a l2-X.Y.Z/ directory; but make sure the package is already
             built, i.e., the "configure; make" step was already performed there).])
        else
             _AC_L2_ERROR([dnl
             Unable to locate OSSP l2 in any system-wide location (see \$PATH).
             Please specify the correct path to either a OSSP l2 installation tree
             (use --with-l2=DIR if you used --prefix=DIR for installing OSSP l2 in
             the past) or to a OSSP l2 source tree (use --with-l2=DIR if DIR is a
             path to a l2-X.Y.Z/ directory; but make sure the package is already
             built, i.e., the "configure; make" step was already performed there).])
        fi
    fi
    dnl #
    dnl #  Check whether the found version is sufficiently new
    dnl #
    _req_version="ifelse([$1],,1.0.0,$1)"
    for _var in _l2_version _req_version; do
        eval "_val=\"\$${_var}\""
        _major=`echo $_val | sed 's/\([[0-9]]*\)\.\([[0-9]]*\)\([[ab.]]\)\([[0-9]]*\)/\1/'`
        _minor=`echo $_val | sed 's/\([[0-9]]*\)\.\([[0-9]]*\)\([[ab.]]\)\([[0-9]]*\)/\2/'`
        _rtype=`echo $_val | sed 's/\([[0-9]]*\)\.\([[0-9]]*\)\([[ab.]]\)\([[0-9]]*\)/\3/'`
        _micro=`echo $_val | sed 's/\([[0-9]]*\)\.\([[0-9]]*\)\([[ab.]]\)\([[0-9]]*\)/\4/'`
        case $_rtype in
            "a" ) _rtype=0 ;;
            "b" ) _rtype=1 ;;
            "." ) _rtype=2 ;;
        esac
        _hex=`echo dummy | awk '{ printf("%d%02d%1d%02d", major, minor, rtype, micro); }' \
              "major=$_major" "minor=$_minor" "rtype=$_rtype" "micro=$_micro"`
        eval "${_var}_hex=\"\$_hex\""
    done
    _AC_L2_VERBOSE([+ Determined Versions:])
    _AC_L2_VERBOSE([    o existing: $_l2_version -> 0x$_l2_version_hex])
    _AC_L2_VERBOSE([    o required: $_req_version -> 0x$_req_version_hex])
    _ok=0
    if test ".$_l2_version_hex" != .; then
        if test ".$_req_version_hex" != .; then
            if test $_l2_version_hex -ge $_req_version_hex; then
                _ok=1
            fi
        fi
    fi
    if test ".$_ok" = .0; then
        _AC_L2_ERROR([dnl
        Found L2 version $_l2_version, but required at least version $_req_version.
        Upgrade L2 under $_l2_location to $_req_version or higher first, please.])
    fi
    dnl #
    dnl #   Perform L2 Sanity Compile Check
    dnl #
    if test ".$with_l2_test" = .yes; then
        _ac_save_CPPFLAGS="$CPPFLAGS"
        _ac_save_CFLAGS="$CFLAGS"
        _ac_save_LDFLAGS="$LDFLAGS"
        _ac_save_LIBS="$LIBS"
        CPPFLAGS="$CPPFLAGS $_l2_cppflags"
        CFLAGS="$CFLAGS $_l2_cflags"
        LDFLAGS="$LDFLAGS $_l2_ldflags"
        LIBS="$LIBS $_l2_libs"
        _AC_L2_VERBOSE([+ Test Build Environment:])
        _AC_L2_VERBOSE([    o CPPFLAGS=\"$CPPFLAGS\"])
        _AC_L2_VERBOSE([    o CFLAGS=\"$CFLAGS\"])
        _AC_L2_VERBOSE([    o LDFLAGS=\"$LDFLAGS\"])
        _AC_L2_VERBOSE([    o LIBS=\"$LIBS\"])
        cross_compile=no
        define(_code1, [dnl
        #include <stdio.h>
        #include <l2.h>
        ])
        define(_code2, [dnl
        int main(int argc, char *argv[])
        {
            FILE *fp;
            if (!(fp = fopen("conftestval", "w")))
                exit(1);
            fprintf(fp, "hmm");
            fclose(fp);
            l2_init();
            l2_kill();
            if (!(fp = fopen("conftestval", "w")))
                exit(1);
            fprintf(fp, "yes");
            fclose(fp);
            exit(0);
        }
        ])
        _AC_L2_VERBOSE([+ Performing Sanity Checks:])
        _AC_L2_VERBOSE([    o pre-processor test])
        AC_TRY_CPP(_code1, _ok=yes, _ok=no)
        if test ".$_ok" != .yes; then
            _AC_L2_ERROR([dnl
            Found OSSP l2 $_l2_version under $_l2_location, but
            was unable to perform a sanity pre-processor check. This means
            the OSSP l2 header l2.h was not found.
            We used the following build environment:
            >> CPP="$CPP"
            >> CPPFLAGS="$CPPFLAGS"
            See config.log for possibly more details.])
        fi
        _AC_L2_VERBOSE([    o link check])
        AC_TRY_LINK(_code1, _code2, _ok=yes, _ok=no)
        if test ".$_ok" != .yes; then
            _AC_L2_ERROR([dnl
            Found OSSP l2 $_l2_version under $_l2_location, but
            was unable to perform a sanity linker check. This means
            the OSSP l2 library libl2.a was not found.
            We used the following build environment:
            >> CC="$CC"
            >> CFLAGS="$CFLAGS"
            >> LDFLAGS="$LDFLAGS"
            >> LIBS="$LIBS"
            See config.log for possibly more details.])
        fi
        _AC_L2_VERBOSE([    o run-time check])
        AC_TRY_RUN(_code1 _code2, _ok=`cat conftestval`, _ok=no, _ok=no)
        if test ".$_ok" != .yes; then
            if test ".$_ok" = .no; then
                _AC_L2_ERROR([dnl
                Found OSSP l2 $_l2_version under $_l2_location, but
                was unable to perform a sanity execution check. This usually
                means that the OSSP l2 shared library libl2.so is present
                but \$LD_LIBRARY_PATH is incomplete to execute a L2 test.
                In this case either disable this test via --without-l2-test,
                or extend \$LD_LIBRARY_PATH, or build OSSP l2 as a static
                library only via its --disable-shared Autoconf option.
                We used the following build environment:
                >> CC="$CC"
                >> CFLAGS="$CFLAGS"
                >> LDFLAGS="$LDFLAGS"
                >> LIBS="$LIBS"
                See config.log for possibly more details.])
            else
                _AC_L2_ERROR([dnl
                Found OSSP l2 $_l2_version under $_l2_location, but
                was unable to perform a sanity run-time check. This usually
                means that the OSSP l2 library failed to work and possibly
                caused a core dump in the test program. In this case it
                is strongly recommended that you re-install OSSP l2 and this
                time make sure that it really passes its "make test" procedure.
                We used the following build environment:
                >> CC="$CC"
                >> CFLAGS="$CFLAGS"
                >> LDFLAGS="$LDFLAGS"
                >> LIBS="$LIBS"
                See config.log for possibly more details.])
            fi
        fi
        _extendvars="ifelse([$4],,yes,$4)"
        if test ".$_extendvars" != .yes; then
            CPPFLAGS="$_ac_save_CPPFLAGS"
            CFLAGS="$_ac_save_CFLAGS"
            LDFLAGS="$_ac_save_LDFLAGS"
            LIBS="$_ac_save_LIBS"
        fi
    else
        _extendvars="ifelse([$4],,yes,$4)"
        if test ".$_extendvars" = .yes; then
            if test ".$_l2_subdir" = .yes; then
                CPPFLAGS="$CPPFLAGS $_l2_cppflags"
                CFLAGS="$CFLAGS $_l2_cflags"
                LDFLAGS="$LDFLAGS $_l2_ldflags"
                LIBS="$LIBS $_l2_libs"
            fi
        fi
    fi
    L2_CPPFLAGS="$_l2_cppflags"
    L2_CFLAGS="$_l2_cflags"
    L2_LDFLAGS="$_l2_ldflags"
    L2_LIBS="$_l2_libs"
    AC_SUBST(L2_CPPFLAGS)
    AC_SUBST(L2_CFLAGS)
    AC_SUBST(L2_LDFLAGS)
    AC_SUBST(L2_LIBS)
    _AC_L2_VERBOSE([+ Final Results:])
    _AC_L2_VERBOSE([    o L2_CPPFLAGS=\"$L2_CPPFLAGS\"])
    _AC_L2_VERBOSE([    o L2_CFLAGS=\"$L2_CFLAGS\"])
    _AC_L2_VERBOSE([    o L2_LDFLAGS=\"$L2_LDFLAGS\"])
    _AC_L2_VERBOSE([    o L2_LIBS=\"$L2_LIBS\"])
fi
if test ".$with_l2" != .no; then
    AC_MSG_RESULT([version $_l2_version, $_l2_type under $_l2_location])
    ifelse([$5], , :, [$5])
else
    AC_MSG_RESULT([no])
    ifelse([$6], , :, [$6])
fi
])

