/*
**  OSSP l2 - Flexible Logging
**  Copyright (c) 2001-2005 Cable & Wireless <http://www.cw.com/>
**  Copyright (c) 2001-2005 The OSSP Project <http://www.ossp.org/>
**  Copyright (c) 2001-2005 Ralf S. Engelschall <rse@engelschall.com>
**
**  This file is part of OSSP l2, a flexible logging library which
**  can be found at http://www.ossp.org/pkg/lib/l2/.
**
**  Permission to use, copy, modify, and distribute this software for
**  any purpose with or without fee is hereby granted, provided that
**  the above copyright notice and this permission notice appear in all
**  copies.
**
**  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
**  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
**  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
**  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
**  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
**  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
**  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
**  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
**  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
**  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
**  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
**  SUCH DAMAGE.
**
**  l2_spec.h: specification common header
*/

#ifndef __L2_SPEC_H__
#define __L2_SPEC_H__

#include "l2.h"

/* internal specification scanner/parser context */
typedef struct {
    const char   *inputptr;  /* input buffer: current reading pointer */
    const char   *inputbuf;  /* input buffer: begin of buffer */
    size_t        inputlen;  /* input buffer: size  of buffer */
    l2_env_t     *env;       /* environment for channel create & error remember */
    l2_channel_t *ch;        /* top-level/root/result channel */
    l2_channel_t *chTmp;     /* temporary channel needed in the parser */
    l2_result_t   rv;        /* return value */
    void         *yyscan;    /* Flex scanner context */
} l2_spec_ctx_t;

/* internal scanner/parser token location */
typedef struct {
    int first;
    int last;
} l2_spec_loc_t;
#define YYLTYPE l2_spec_loc_t

/* support for automatic location tracking by Bison */
#define first_line   first
#define first_column first
#define last_line    last
#define last_column  last

/* error reporting function */
extern void l2_spec_error(l2_spec_ctx_t *ctx, l2_result_t rv, YYLTYPE *loc, const char *fmt, ...);

#endif /* __L2_SPEC_H__ */
