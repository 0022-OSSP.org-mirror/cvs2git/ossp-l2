/*
**  OSSP l2 - Flexible Logging
**  Copyright (c) 2001-2005 Cable & Wireless <http://www.cw.com/>
**  Copyright (c) 2001-2005 The OSSP Project <http://www.ossp.org/>
**  Copyright (c) 2001-2005 Ralf S. Engelschall <rse@engelschall.com>
**
**  This file is part of OSSP l2, a flexible logging library which
**  can be found at http://www.ossp.org/pkg/lib/l2/.
**
**  Permission to use, copy, modify, and distribute this software for
**  any purpose with or without fee is hereby granted, provided that
**  the above copyright notice and this permission notice appear in all
**  copies.
**
**  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
**  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
**  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
**  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
**  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
**  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
**  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
**  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
**  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
**  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
**  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
**  SUCH DAMAGE.
**
**  l2_ch_fd.c: file descriptor channel implementation
*/

#include "l2.h"
#include <unistd.h>

/* declare private channel configuration */
typedef struct {
    int fd;
} l2_ch_fd_t;

/* create channel */
static l2_result_t hook_create(l2_context_t *ctx, l2_channel_t *ch)
{
    l2_ch_fd_t *cfg;

    /* allocate private channel configuration */
    if ((cfg = (l2_ch_fd_t *)malloc(sizeof(l2_ch_fd_t))) == NULL)
        return L2_ERR_MEM;

    /* initialize configuration with reasonable defaults */
    cfg->fd = -1;

    /* link private channel configuration into channel context */
    ctx->vp = cfg;

    return L2_OK;
}

/* configure channel */
static l2_result_t hook_configure(l2_context_t *ctx, l2_channel_t *ch, const char *fmt, va_list ap)
{
    l2_ch_fd_t *cfg;
    l2_param_t pa[2];
    l2_result_t rv;
    l2_env_t *env;

    /* parameter checks */
    if ((cfg = (l2_ch_fd_t *)ctx->vp) == NULL)
        return L2_ERR_ARG;

    /* feed and call generic parameter parsing engine */
    L2_PARAM_SET(pa[0], fd, INT, &cfg->fd);
    L2_PARAM_END(pa[1]);
    l2_channel_env(ch, &env);
    rv = l2_util_setparams(env, pa, fmt, ap);

    return rv;
}

/* open channel */
static l2_result_t hook_open(l2_context_t *ctx, l2_channel_t *ch)
{
    l2_ch_fd_t *cfg;

    /* parameter checks */
    if ((cfg = (l2_ch_fd_t *)ctx->vp) == NULL)
        return L2_ERR_ARG;
    if ((cfg->fd == -1))
        return L2_ERR_ARG;

    return L2_OK;
}

/* write to channel */
static l2_result_t hook_write(l2_context_t *ctx, l2_channel_t *ch,
                              l2_level_t level, const char *buf, size_t buf_size)
{
    l2_ch_fd_t *cfg;

    /* parameter checks */
    if ((cfg = (l2_ch_fd_t *)ctx->vp) == NULL)
        return L2_ERR_ARG;
    if (cfg->fd == -1)
        return L2_ERR_ARG;

    /* write message to channel fd */
    if (write(cfg->fd, buf, buf_size) == -1)
        return L2_ERR_SYS;

    return L2_OK;
}

/* flush channel */
static l2_result_t hook_flush(l2_context_t *ctx, l2_channel_t *ch)
{
    /* NOP for this channel, because Unix I/O files are unbuffered! */

    return L2_OK;
}

/* close channel */
static l2_result_t hook_close(l2_context_t *ctx, l2_channel_t *ch)
{
    l2_ch_fd_t *cfg;

    /* parameter checks */
    if ((cfg = (l2_ch_fd_t *)ctx->vp) == NULL)
        return L2_ERR_ARG;
    if (cfg->fd == -1)
        return L2_ERR_ARG;

    /* close channel fd   */
    /* nothing to close   */

    return L2_OK;
}

/* destroy channel */
static l2_result_t hook_destroy(l2_context_t *ctx, l2_channel_t *ch)
{
    /* parameter checks */
    if (ctx->vp == NULL)
        return L2_ERR_ARG;

    /* destroy channel configuration */
    free(ctx->vp);

    return L2_OK;
}

/* exported channel handler structure */
l2_handler_t l2_handler_fd = {
    "fd",
    L2_CHANNEL_OUTPUT,
    hook_create,
    hook_configure,
    hook_open,
    hook_write,
    hook_flush,
    hook_close,
    hook_destroy
};

