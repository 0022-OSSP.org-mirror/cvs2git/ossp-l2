/*
**  OSSP l2 - Flexible Logging
**  Copyright (c) 2001-2005 Cable & Wireless <http://www.cw.com/>
**  Copyright (c) 2001-2005 The OSSP Project <http://www.ossp.org/>
**  Copyright (c) 2001-2005 Ralf S. Engelschall <rse@engelschall.com>
**
**  This file is part of OSSP l2, a flexible logging library which
**  can be found at http://www.ossp.org/pkg/lib/l2/.
**
**  Permission to use, copy, modify, and distribute this software for
**  any purpose with or without fee is hereby granted, provided that
**  the above copyright notice and this permission notice appear in all
**  copies.
**
**  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
**  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
**  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
**  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
**  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
**  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
**  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
**  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
**  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
**  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
**  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
**  SUCH DAMAGE.
**
**  l2_spec.c: channel tree specification support
*/

#include "l2.h"
#include "l2_p.h"
#include "l2_spec.h"

/* prototypes for Flex-generated scanner */
extern int  l2_spec_lex_init(void *);
extern int  l2_spec_lex_destroy(void *);
extern void l2_spec_set_extra(void *, void *);

/* prototypes for Bison-generated parser */
extern int  l2_spec_parse(void *);

/* build a channel tree according to a textual specification */
l2_result_t l2_spec(l2_channel_t **ch, l2_env_t *env, const char *spec, ...)
{
    va_list ap;
    l2_result_t rv;

    /* pass-through to va_list-based variant */
    va_start(ap, spec);
    rv = l2_vspec(ch, env, spec, ap);
    va_end(ap);

    return rv;
}

/* build a channel tree according to a textual specification (va_list variant) */
l2_result_t l2_vspec(l2_channel_t **ch, l2_env_t *env, const char *spec, va_list ap)
{
    l2_spec_ctx_t ctx;
    void *yyscan;
    char *specstr;

    /* on-the-fly create or take over specification string */
    if ((specstr = l2_util_vasprintf(spec, ap)) == NULL)
        return L2_ERR_ARG;

    /* initialize scanner */
    l2_spec_lex_init(&yyscan);
    l2_spec_set_extra(&ctx, yyscan);

    /* establish our own context which is passed
       through the parser and scanner */
    ctx.yyscan   = yyscan;
    ctx.inputptr = specstr;
    ctx.inputbuf = specstr;
    ctx.inputlen = strlen(specstr);
    ctx.env      = env;
    ctx.ch       = NULL;
    ctx.chTmp    = NULL;
    ctx.rv       = L2_OK;

    /* start the parser loop */
    if (l2_spec_parse(&ctx))
        ctx.rv = (ctx.rv == L2_OK ? L2_ERR_INT : ctx.rv);

    /* destroy scanner */
    l2_spec_lex_destroy(yyscan);

    /* destroy specification string */
    free(specstr);

    /* provide root/top-level channel as result */
    *ch = ctx.ch;

    return ctx.rv;
}

/* remember a specification parsing error (used internally) */
void l2_spec_error(l2_spec_ctx_t *ctx, l2_result_t rv, YYLTYPE *loc, const char *fmt, ...)
{
    va_list ap;
    const char *cpF, *cpL;
    const char *cpP, *cpE;
    int line, column;
    char *cpBuf;
    char *cp;
    int n;

    /* determine first and last positions of token */
    cpF = ctx->inputbuf+loc->first;
    cpL = ctx->inputbuf+loc->last;

    /* determine epilog and prolog of token */
    cpP = cpF-4;
    if (cpP < ctx->inputbuf)
        cpP = ctx->inputbuf;
    cpE = cpL+4;
    if (cpE > ctx->inputbuf+ctx->inputlen)
        cpE = ctx->inputbuf+ctx->inputlen;

    /* calculate line and column of token */
    line   = 1;
    column = 1;
    for (cp = (char *)ctx->inputbuf; cp < (ctx->inputbuf+ctx->inputlen) && cp != cpF; cp++) {
        column++;
        if (*cp == '\n') {
            column = 1;
            line++;
        }
    }

    /* extract token context with mark token borders */
    if ((cpBuf = malloc((cpE-cpP)+2+1)) == NULL)
        return;
    cp = cpBuf;
    n = cpF-cpP;
    memcpy(cp, cpP, n); cp += n;
    *cp++ = '<';
    n = cpL-cpF;
    memcpy(cp, cpF, n); cp += n;
    *cp++ = '>';
    n = cpE-cpL;
    memcpy(cp, cpL, n); cp += n;
    *cp++ = '\0';

    /* remember error */
    va_start(ap, fmt);
    if ((cp = l2_util_vasprintf(fmt, ap)) != NULL) {
        l2_env_errorinfo(ctx->env, rv, "line %d, column %d: `%s'; %s",
                         line, column, cpBuf, cp);
        free(cp);
    }
    else
        l2_env_errorinfo(ctx->env, rv, "line %d, column %d: `%s'; N.A.",
                         line, column, cpBuf);
    va_end(ap);
    ctx->rv = rv;

    /* cleanup */
    free(cpBuf);

    return;
}

