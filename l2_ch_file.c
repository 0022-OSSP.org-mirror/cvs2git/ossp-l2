/*
**  OSSP l2 - Flexible Logging
**  Copyright (c) 2001-2005 Cable & Wireless <http://www.cw.com/>
**  Copyright (c) 2001-2005 The OSSP Project <http://www.ossp.org/>
**  Copyright (c) 2001-2005 Ralf S. Engelschall <rse@engelschall.com>
**
**  This file is part of OSSP l2, a flexible logging library which
**  can be found at http://www.ossp.org/pkg/lib/l2/.
**
**  Permission to use, copy, modify, and distribute this software for
**  any purpose with or without fee is hereby granted, provided that
**  the above copyright notice and this permission notice appear in all
**  copies.
**
**  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
**  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
**  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
**  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
**  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
**  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
**  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
**  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
**  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
**  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
**  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
**  SUCH DAMAGE.
**
**  l2_ch_file.c: file channel implementation
*/

#include "l2.h"

#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>

/* declare private channel configuration */
typedef struct {
    int   fd;
    char *path;
    int   append;
    int   trunc;
    int   perm;
    int   jitter;
    int   jittercount;
    int   monitor;
    long  monitortime;
    dev_t monitordev;
    ino_t monitorino;
} l2_ch_file_t;

/* open channel file */
static void openchfile(l2_context_t *ctx, l2_channel_t *ch, int mode)
{
    l2_ch_file_t *cfg = (l2_ch_file_t *)ctx->vp;
    mode_t mask;
    struct timeval tv;
    struct stat st;

#ifdef O_LARGEFILE
    /* Support for 1996-03-20 addition to Single UNIX Specification for
     * systems that still require the Transitional Extensions to System
     * Interfaces to support arbitrary file sizes (LFS >2GiB)
     */
    mode |= O_LARGEFILE;
#endif

    /* open channel file */
    mask = umask(0);
    cfg->fd = open(cfg->path, mode, cfg->perm);
    umask(mask);

    /* prepare jittering counter */
    cfg->jittercount = 0;

    /* prepare monitoring time and stat */
    if (cfg->monitor >= 1) {
        if (gettimeofday(&tv, NULL) != -1)
            cfg->monitortime = tv.tv_sec;
        else
            cfg->monitortime = 0;
        if (   (cfg->fd != -1)
            && (fstat(cfg->fd, &st) != -1)) {
            cfg->monitordev = st.st_dev;
            cfg->monitorino = st.st_ino;
        }
        else {
            cfg->monitordev = 0;
            cfg->monitorino = 0;
        }
    }
}

/* create channel */
static l2_result_t hook_create(l2_context_t *ctx, l2_channel_t *ch)
{
    l2_ch_file_t *cfg;

    /* allocate private channel configuration */
    if ((cfg = (l2_ch_file_t *)malloc(sizeof(l2_ch_file_t))) == NULL)
        return L2_ERR_ARG;

    /* initialize configuration with reasonable defaults */
    cfg->fd          = -1;
    cfg->path        = NULL;
    cfg->append      = -1;
    cfg->trunc       = -1;
    cfg->perm        = (S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH);
    cfg->jitter      = 0;
    cfg->jittercount = 0;
    cfg->monitor     = 0;
    cfg->monitortime = 0;
    cfg->monitordev  = 0;
    cfg->monitorino  = 0;

    /* link private channel configuration into channel context */
    ctx->vp = cfg;

    return L2_OK;
}

/* configure channel */
static l2_result_t hook_configure(l2_context_t *ctx, l2_channel_t *ch, const char *fmt, va_list ap)
{
    l2_ch_file_t *cfg = (l2_ch_file_t *)ctx->vp;
    l2_param_t pa[7];
    l2_result_t rv;
    l2_env_t *env;

    /* feed and call generic parameter parsing engine */
    L2_PARAM_SET(pa[0], path,    STR, &cfg->path);
    L2_PARAM_SET(pa[1], append,  INT, &cfg->append);
    L2_PARAM_SET(pa[2], trunc,   INT, &cfg->trunc);
    L2_PARAM_SET(pa[3], perm,    INT, &cfg->perm);
    L2_PARAM_SET(pa[4], jitter,  INT, &cfg->jitter);
    L2_PARAM_SET(pa[5], monitor, INT, &cfg->monitor);
    L2_PARAM_END(pa[6]);
    l2_channel_env(ch, &env);
    rv = l2_util_setparams(env, pa, fmt, ap);

    return rv;
}

/* open channel */
static l2_result_t hook_open(l2_context_t *ctx, l2_channel_t *ch)
{
    l2_ch_file_t *cfg = (l2_ch_file_t *)ctx->vp;

    /* "append" backward compatibility; only cfg->trunc is used in the code
     * make sure append/trunc either both use defaults, both are set different, or only one is set
     *
     * truth table for user input,       append, trunc => resulting trunc
     *                 -----------------+------+------+------------------
     *                                       -1     -1      0 (default)
     *                 trunc=0               -1      0      0
     *                 trunc=1               -1      1      1
     *                 append=0               0     -1      1
     *                 append=0, trunc=0      0      0      ERROR
     *                 append=0, trunc=1      0      1      1
     *                 append=1               1     -1      0
     *                 append=1, trunc=0      1      0      0
     *                 append=1, trunc=1      1      1      ERROR
     */
    if (cfg->append >= 1)
        cfg->append = 1; /* reduce to -1 (undef), 0 (no), 1 (yes) */
    if (cfg->trunc >= 1)
        cfg->trunc = 1; /* reduce to -1 (undef), 0 (no), 1 (yes) */
    if (   cfg->append != -1
        && cfg->trunc != -1
        && cfg->append == cfg->trunc) /* collision */
        return L2_ERR_USE;
    if (   cfg->trunc == -1)
        cfg->trunc = (1 - cfg->append) & 1;

    /* make sure jitter count is positive number */
    if (cfg->jitter < 0)
        return L2_ERR_USE;

    /* make sure monitor time is positive number */
    if (cfg->monitor < 0)
        return L2_ERR_USE;

    /* make sure a path was set */
    if (cfg->path == NULL)
        return L2_ERR_USE;

    /* open channel file */
    if (cfg->trunc == 1)
        openchfile(ctx, ch, O_WRONLY|O_CREAT|O_TRUNC);
    else
        openchfile(ctx, ch, O_WRONLY|O_CREAT|O_APPEND);

    if (cfg->fd == -1)
        return L2_ERR_SYS;

    return L2_OK;
}

/* write to channel */
static l2_result_t hook_write(l2_context_t *ctx, l2_channel_t *ch,
                              l2_level_t level, const char *buf, size_t buf_size)
{
    l2_ch_file_t *cfg = (l2_ch_file_t *)ctx->vp;
    l2_result_t rc = L2_OK;
    int reopen = 0;
    struct timeval tv;
    struct stat st;

    /* if jittering, count writes and reopen file if jitter threshold is reached or exceeded */
    if (cfg->jitter >= 1) {
        cfg->jittercount++;
        if (cfg->jittercount >= cfg->jitter) {
            cfg->jittercount = 0;
            reopen = 1;
        }
    }

    /* if monitoring, from time to time check for a renamed log and reopen file on detection */
    if (cfg->monitor >= 1) {
        int dostat = 0;
        if (gettimeofday(&tv, NULL) != -1) {
            if ((tv.tv_sec - cfg->monitortime) >= cfg->monitor) {
                cfg->monitortime = tv.tv_sec;
                dostat = 1;
            }
        }
        else {
            dostat = 1;
        }
        if (dostat == 1) {
            if (stat(cfg->path, &st) == -1) {
                reopen = 1;
            }
            else {
                if (   (cfg->monitordev != st.st_dev)
                    || (cfg->monitorino != st.st_ino)) {
                    reopen = 1;
                }
            }
        }
    }

    /* close for reopen if required */
    if (reopen == 1 && cfg->fd != -1) {
        close(cfg->fd);
        cfg->fd = -1;
    }

    /* open if required */
    if (cfg->fd == -1) {
        openchfile(ctx, ch, O_WRONLY|O_CREAT|O_APPEND);
    }

    if (cfg->fd == -1)
        return L2_ERR_SYS;

    /* write message to channel file */
    if (write(cfg->fd, buf, buf_size) == -1)
        rc = L2_ERR_SYS;

    return rc;
}

/* close channel */
static l2_result_t hook_close(l2_context_t *ctx, l2_channel_t *ch)
{
    l2_ch_file_t *cfg = (l2_ch_file_t *)ctx->vp;

    /* close channel file */
    close(cfg->fd);
    cfg->fd = -1;

    return L2_OK;
}

/* destroy channel */
static l2_result_t hook_destroy(l2_context_t *ctx, l2_channel_t *ch)
{
    l2_ch_file_t *cfg = (l2_ch_file_t *)ctx->vp;

    /* destroy channel configuration */
    if (cfg->path != NULL)
        free(cfg->path);
    free(cfg);

    return L2_OK;
}

/* exported channel handler structure */
l2_handler_t l2_handler_file = {
    "file",
    L2_CHANNEL_OUTPUT,
    hook_create,
    hook_configure,
    hook_open,
    hook_write,
    NULL,
    hook_close,
    hook_destroy
};

