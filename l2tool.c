/*
**  OSSP l2 - Flexible Logging
**  Copyright (c) 2001-2005 Cable & Wireless <http://www.cw.com/>
**  Copyright (c) 2001-2005 The OSSP Project <http://www.ossp.org/>
**  Copyright (c) 2001-2005 Ralf S. Engelschall <rse@engelschall.com>
**
**  This file is part of OSSP l2, a flexible logging library which
**  can be found at http://www.ossp.org/pkg/lib/l2/.
**
**  Permission to use, copy, modify, and distribute this software for
**  any purpose with or without fee is hereby granted, provided that
**  the above copyright notice and this permission notice appear in all
**  copies.
**
**  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
**  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
**  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
**  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
**  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
**  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
**  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
**  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
**  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
**  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
**  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
**  SUCH DAMAGE.
**
**  l2tool.c: command line tool
*/

#include <stdio.h>
#include "l2.h"

#include <unistd.h>

static void die(l2_env_t *env, l2_result_t rv, char *fmt, ...)
{
    va_list ap;
    char *error;

    va_start(ap, fmt);
    fprintf(stderr, "l2tool:ERROR: ");
    vfprintf(stderr, fmt, ap);
    error = l2_env_strerror(env, rv);
    fprintf(stderr, " (%s)\n", error);
    va_end(ap);
    exit(1);
}

static void usage(void)
{
    fprintf(stderr, "l2tool:ERROR: invalid command line\n");
    fprintf(stderr, "l2tool:USAGE: l2tool [-s sleepsec] <specification>\n");
    exit(1);
}

int main(int argc, char *argv[])
{
    l2_channel_t *ch;
    char *spec;
    l2_result_t rv;
    l2_env_t *env;
    char *cpLevel;
    char caBuf[2048];
    int nBuf;
    char *cpMsg;
    char *cp;
    unsigned int nLevel;
    int option;
    int nSleep = 0;

    /* command line parsing */
    while ((option = getopt(argc, argv, "s:")) != EOF) {
        switch ((char) option) {
            case 's':
                nSleep = atoi(optarg);
                break;
            default:
                usage();
        /* NOTREACHED */
        }
    }
    if (argc - optind != 1)
        usage();
    spec = argv[optind];

    /* create environment */
    if ((rv = l2_env_create(&env)) != L2_OK)
        die(env, rv, "failed to create environment");

    /* create channel tree */
    if ((rv = l2_spec(&ch, env, spec)) != L2_OK)
        die(env, rv, "failed to parse specification");

    /* open channel tree */
    if ((rv = l2_channel_open(ch)) != L2_OK)
        die(env, rv, "failed to open channel tree");

    /* loop over stdin */
    while (fgets(caBuf, sizeof(caBuf), stdin) != NULL) {
        /* strip newline */
        nBuf = strlen(caBuf);
        caBuf[nBuf-1] = '\0';

        /* parse logging level */
        nLevel  = L2_LEVEL_INFO;
        cpLevel = "info";
        cpMsg   = caBuf;
        if (caBuf[0] == '<') {
            if ((cp = strchr(caBuf+1, '>')) == NULL)
                die(env, rv, "unterminated level prefix");
            *cp++ = '\0';
            cpLevel = caBuf+1;
            cpMsg   = cp;
        }
        if (l2_util_s2l(cpLevel, strlen(cpLevel), ',', &nLevel) != L2_OK)
            die(env, rv, "invalid level prefix");

        /* perform log operation(s) */
        if ((rv = l2_channel_log(ch, nLevel, "%s", cpMsg)) != L2_OK)
            die(env, rv, "failed to log message to channel tree");

        /* artifical delay */
        sleep(nSleep);
    }

    /* destroy channel tree */
    if ((rv = l2_channel_destroy(ch)) != L2_OK)
        die(env, rv, "failed to destroy channel tree");

    /* destroy environment */
    if ((rv = l2_env_destroy(env)) != L2_OK)
        die(env, rv, "failed to destroy environment");

    return 0;
}

