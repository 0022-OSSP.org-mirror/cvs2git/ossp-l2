/*
**  OSSP l2 - Flexible Logging
**  Copyright (c) 2001-2005 Cable & Wireless <http://www.cw.com/>
**  Copyright (c) 2001-2005 The OSSP Project <http://www.ossp.org/>
**  Copyright (c) 2001-2005 Ralf S. Engelschall <rse@engelschall.com>
**
**  This file is part of OSSP l2, a flexible logging library which
**  can be found at http://www.ossp.org/pkg/lib/l2/.
**
**  Permission to use, copy, modify, and distribute this software for
**  any purpose with or without fee is hereby granted, provided that
**  the above copyright notice and this permission notice appear in all
**  copies.
**
**  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
**  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
**  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
**  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
**  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
**  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
**  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
**  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
**  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
**  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
**  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
**  SUCH DAMAGE.
**
**  l2_p.h: private C API
*/

#ifndef __L2_P_H__
#define __L2_P_H__

/* include standard headers */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

/* include private headers */
#include "l2.h"
#include "l2_config.h"
#include "l2_ut_format.h"
#include "l2_ut_pcre.h"
#include "l2_ut_sa.h"

#ifdef HAVE_SYS_TIME_H /* must come after include l2_config.h */
#include <sys/time.h>
#endif

/* assertion and tracing support */
#ifndef DEBUG
#define NDEBUG
#endif
#include <assert.h>
#ifndef NDEBUG
#define TRACE(str)  fprintf(stderr, "%s:%d: %s\n", __FILE__, __LINE__, str)
#else
#define TRACE(expr) ((void)0)
#endif

/* memory debugging support */
#if defined(HAVE_DMALLOC_H) && defined(WITH_DMALLOC)
#include "dmalloc.h"
#endif

/* handy cleanup macros */
#define cu(value) \
    do { rv = value; \
         goto cus; } while (0)
#define cu_on(expr,value) \
    if (expr) \
        cu(value)

/* some hard-coded sizes :-( */
#define L2_MAX_OUTSIZE          4098 /* this value is for compatiblity with previous versions of L2 */
#define L2_MAX_MSGSIZE          L2_MAX_OUTSIZE - 2 /* leave room for CR/LF although OSSP/UNIX use LF only */
#define L2_MSG_BUFSIZE          L2_MAX_OUTSIZE + 1 /* reserve space for convenient trailing NUL */
#define L2_MAX_FORMATTERS       128
#define L2_MAX_HANDLERS         128
#define L2_BROKEN_TIMER          -1
#define L2_BUFFER_TIMER ITIMER_REAL /* for [s|g]etitimer() and alarm() */

/* internal channel state */
typedef enum {
    L2_CHSTATE_CREATED,
    L2_CHSTATE_OPENED
} l2_chstate_t;

/* channel object structure */
struct l2_channel_st {
    l2_env_t     *env;
    l2_chstate_t  state;
    l2_channel_t *parent;
    l2_channel_t *sibling;
    l2_channel_t *child;
    l2_context_t  context;
    l2_handler_t  handler;
    unsigned int  levelmask;
    unsigned int  flushmask;
};

/* formatter entry structure */
typedef struct {
    l2_formatter_t cb;
    void *ctx;
    char id;
} l2_formatter_entry_t;

/* environment object structure */
struct l2_env_st {
    unsigned int          levelmask;
    unsigned int          flushmask;
    int                   interval;
    l2_formatter_entry_t  formatters[L2_MAX_FORMATTERS];
    l2_handler_t         *handlers[L2_MAX_HANDLERS];
    char                  message[L2_MSG_BUFSIZE];
    char                  szError[1024];
    char                  szErrorInfo[512];
    l2_result_t           rvErrorInfo;
};

/* variable argument handling taking care on argument passing conventions */
#define _va_type_recv_char    int
#define _va_type_cast_char    char
#define _va_type_recv_short   int
#define _va_type_cast_short   short
#define _va_type_recv_int     int
#define _va_type_cast_int     int
#define _va_type_recv_long    long
#define _va_type_cast_long    long
#define _va_type_recv_float   double
#define _va_type_cast_float   double
#define _va_type_recv_double  double
#define _va_type_cast_double  double
#define _va_type_recv_charptr void *
#define _va_type_cast_charptr char *
#define _va_type_recv_voidptr void *
#define _va_type_cast_voidptr void *
#define va_get(ap,type) (_va_type_cast_##type)va_arg((ap),_va_type_recv_##type)

#endif /* __L2_P_H__ */

